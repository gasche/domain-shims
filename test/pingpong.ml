(* TEST
*)

let r = ref (Some 0)

let () = Gc.minor ()

(* Note: this test is modified from the upstream test.

   The test runs two functions in parallel, [even] and [odd], which
   collaborate to increment a reference from 0 to [limit] by each
   incrementing it in turn. This requires [limit] interleavings of
   control from one domain to the next. Each function loops when it is
   the other function's turn to perform the increment.

   In the upstream implementation of the benchmark, the "loop while
   waiting" logic contained an unused allocation (`let _ = [!r] in
   ...`), probably as a way to ensure that control is eventually
   transferred from one domain to the next (presumably this test was
   written before automatic poll point insertion was added).

   This is however far from enough with a our Thread implementation,
   which only guarantees thread preemption every 50ms in absence of
   explicit yields. To perform the 100_000 interleavings of control
   necessary for this test, we would need 50*100 seconds, which is
   above one hour.
   Turning the unused allocation into a Thread.yield makes the test
   instantatenous. Below we tweaked the test to call Unix.sleepf
   0. instead, which releases the master lock but does not
   systematically yield control to other threads.
*)

let rec even lim put =
  match !r with
  | Some n when n = lim -> ()
  | (Some n) when n mod 2 == 0 ->
     let next = Some (n + 1) in
     put next;
     even lim put
  | _ -> Unix.sleepf 0.; even lim put

let rec odd lim put =
  match !r with
  | Some n when n = lim -> ()
  | (Some n) when n mod 2 == 1 ->
     let next = Some (n + 1) in
     put next;
     odd lim put
  | _ -> Unix.sleepf 0.; odd lim put

let go n put =
  r := Some 0;
  let d = Domain.spawn (fun () -> even n put) in
  odd n put;
  (match !r with
  | Some n ->
     Printf.printf "%d\n%!" n
  | None ->
     assert false);
  Domain.join d


let () =
  go 100_000 (fun x -> r := x)
