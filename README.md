This repository contains a self-contained implementation of the Domain
module for OCaml 4.

## Motivation

`Domain` is a new module of the OCaml 5 standard library that exposes
the Multicore OCaml runtime's ability to run OCaml computations in
parallel.

With `domain-shims`, you can compile run code that was written for
OCaml 5 using the Domain module under OCaml 4. This could make it
easier to gradually transition to OCaml 5, while preserving some sort
of compatibility with previous versions, instead of forcing all your
users to migrate in one go.

The `domain-shims` implementation of course does not provide true
parallelism (as OCaml 5 does): its "fake domains" are implemented
using the `Thread` module of OCaml 4, which never runs threads in
parallel. In particular, `domain-shims`-using code will never get
faster by spawning more domains.

We expect our implementation to be correct, in the sense that the
observed behavior of `domain-shims` programs are also valid behaviors
according to the OCaml 5 specification of domains. You can think of
our non-parallel implementation as one extreme, unlikely and
inefficient, but valid, interleaving of parallel computations.

Except for the lack of parallelism, the `domain-shims` implementation
is otherwise pretty reasonable and fast enough: it should get you
a valid result, in about the same time as OCaml 5 running on
a single-core machine. (Usual time- and memory-performance variations
between OCaml 4 and OCaml 5 of course apply here as well.)


## Limitations

### No Effect support

OCaml 5 also provides effect handlers as a new `Effect` module of the
standard library. We do not provide an implementation of `Effect`,
which is much harder to emulate using regular OCaml 4.

Someone could experiment with an implementation using the
[delimcc](https://opam.ocaml.org/packages/delimcc/) library for
delimited control. (Please do this and report! It is probably fun.) We
expect that the result would be much slower than the OCaml 5
implementation, probably by an order of magnitude. This would be okay
for toy experiments. But it would not be good enough to seriously run
libraries relying on effect handlers for performance-sensitive
operations, such as Domainslib or Eio.

The other option would be to implement shim versions of Effect-using
libraries. We are considering providing a shim for Domainslib in
particular, which is very easy to do without emulating effects at
all. This general approach sounds scary (no limit to the number of
programs you need to provide shims for), but remember that OCaml
5 users are not supposed to use effect handlers too much yet, it was
only made available for lightweight concurrent schedulers.


## Testing

We tested this code by importing the `parallel` tests of the OCaml
compiler distribution. (Except for a few tests also using
`Effect`). They pass without changes, except for
[pingpong.ml](./test/pingpong.ml) which required a small tweak to
terminate in reasonable time, as it assumes parallelism in practice --
see the comments in the code for more details.
