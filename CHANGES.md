#v0.1.1
---

Remove dependency on `threads` when compiling with OCaml >= 5

#v0.1.0
---

Initial release
